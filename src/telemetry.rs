use std::sync::Once;

use tracing_subscriber::fmt::format;
use tracing_subscriber::EnvFilter;

static INIT: Once = Once::new();

pub fn init() {
    INIT.call_once(|| {
        if std::env::var("RUST_LOG").is_err() {
            std::env::set_var("RUST_LOG", "info");
        }
        tracing_subscriber::fmt::fmt()
            .event_format(format().pretty())
            .with_env_filter(EnvFilter::from_default_env())
            .init();
    })
}
