use std::path::PathBuf;

use anyhow::Result;

use crate::database::{Blob, Object};
use crate::repository::Repository;

use super::ProcessInfo;

#[derive(clap::Args, Debug)]
pub struct Args {
    pub paths: Vec<PathBuf>,
}

pub fn execute(args: Args, process_info: &mut ProcessInfo) -> Result<()> {
    let repo = Repository::new(process_info.root.join(".git"));

    let workspace = repo.workspace();
    let database = repo.database();
    let mut index = repo.index_for_update()?;

    let files = args
        .paths
        .iter()
        .map(|path| workspace.list_files(path))
        .collect::<Result<Vec<_>>>()?;
    let files = files.iter().flatten();

    for file in files {
        let data = file.read()?;

        let mut blob = Blob::new(data);
        database.store(&mut blob)?;
        index.add(&file, blob.oid())?;
    }

    index.write_updates()?;

    Ok(())
}
