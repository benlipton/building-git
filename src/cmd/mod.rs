pub mod add;
pub mod commit;
pub mod init;

use std::io::{Read, Write};
use std::path::PathBuf;

use anyhow::Result;
use clap::Parser;

pub struct ProcessInfo<'a> {
    pub root: PathBuf,
    pub stdin: Box<dyn Read + 'a>,
    pub stdout: Box<dyn Write + 'a>,
    pub stderr: Box<dyn Write + 'a>,
}

impl<'a> ProcessInfo<'a> {
    pub fn from_env() -> Self {
        let root = ".".into();
        Self {
            root,
            stdin: Box::new(std::io::stdin()),
            stdout: Box::new(std::io::stdout()),
            stderr: Box::new(std::io::stderr()),
        }
    }
}

#[derive(Parser, Debug)]
pub enum Cli {
    Init(init::Args),
    Commit(commit::Args),
    Add(add::Args),
}

pub fn parse() -> Cli {
    Cli::parse()
}

pub fn execute(cmd: Cli, process_info: &mut ProcessInfo) -> Result<()> {
    match cmd {
        Cli::Init(args) => init::execute(args, process_info),
        Cli::Commit(args) => commit::execute(args, process_info),
        Cli::Add(args) => add::execute(args, process_info),
    }
}
