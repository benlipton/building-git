use std::io::{self, Read};

use anyhow::{anyhow, Result};
use rustc_serialize::hex::ToHex;
use time::OffsetDateTime;

use crate::database::{Author, Commit, Object, Tree, TreeFile};
use crate::repository::Repository;

use super::ProcessInfo;

#[derive(clap::Args, Debug)]
pub struct Args {
    #[clap(long, env = "GIT_AUTHOR_NAME")]
    author_name: String,
    #[clap(long, env = "GIT_AUTHOR_EMAIL")]
    author_email: String,
}

pub fn execute(args: Args, process_info: &mut ProcessInfo) -> Result<()> {
    let repo = Repository::new(process_info.root.join(".git"));

    let index = repo.index()?;
    let refs = repo.refs();
    let database = repo.database();

    let entries = index
        .iter()
        .map(|entry| TreeFile::new(&entry.path, &entry.oid.to_hex(), entry.mode));

    let mut root = Tree::build(entries)?;
    root.traverse(&|tree| database.store(tree))?;

    let parent = refs.read_head()?;
    let name = args.author_name;
    let email = args.author_email;
    let timestamp = OffsetDateTime::try_now_local().unwrap_or_else(|_| OffsetDateTime::now_utc());
    let author = Author::new(&name, &email, timestamp);

    let mut message = String::new();
    io::stdin().read_to_string(&mut message)?;

    let mut commit = Commit::new(
        parent.to_owned(),
        root.oid().to_owned(),
        author,
        message.clone(),
    );
    database.store(&mut commit)?;

    let first_line = message.lines().next().ok_or(anyhow!("Empty message"))?;
    let commit_oid = commit.oid();

    refs.update_head(commit_oid)?;

    let is_root = if parent.is_none() {
        "(root-commit) "
    } else {
        ""
    };
    write!(
        process_info.stdout,
        "[{}{}] {}",
        is_root, commit_oid, first_line
    )?;

    Ok(())
}
