use std::fs;
use std::path::PathBuf;

use anyhow::{Context, Result};

use super::ProcessInfo;

#[derive(clap::Args, Debug)]
pub struct Args {
    #[clap(default_value = ".")]
    pub root: PathBuf,
}

pub fn execute(args: Args, process_info: &mut ProcessInfo) -> Result<()> {
    let git = args.root.join(".git");
    fs::create_dir_all(&git).with_context(|| format!("Failed to create {}", git.display()))?;
    let git = fs::canonicalize(git)?;

    let create = |dirname| {
        let path = git.join(dirname);
        fs::create_dir_all(&path).with_context(|| format!("Failed to create {}", path.display()))
    };
    create("objects")?;
    create("refs")?;
    write!(
        process_info.stdout,
        "Initialized empty Jit repository in {}",
        git.display()
    )?;
    Ok(())
}
