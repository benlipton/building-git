use std::path::PathBuf;

use anyhow::Result;

use crate::database::Database;
use crate::index::Index;
use crate::refs::Refs;
use crate::workspace::Workspace;

#[derive(Debug)]
pub struct Repository {
    git_path: PathBuf,
}

impl Repository {
    #[tracing::instrument]
    pub fn new(git_path: PathBuf) -> Self {
        Self { git_path }
    }

    #[tracing::instrument]
    pub fn database(&self) -> Database {
        let path = self.git_path.join("objects");
        Database::new(path)
    }

    #[tracing::instrument]
    pub fn index(&self) -> Result<Index> {
        Index::load(self.git_path.join("index"))
    }

    #[tracing::instrument]
    pub fn index_for_update(&self) -> Result<Index> {
        Index::load_for_update(self.git_path.join("index"))
    }

    #[tracing::instrument]
    pub fn refs(&self) -> Refs {
        Refs::new(self.git_path.clone())
    }

    #[tracing::instrument]
    pub fn workspace(&self) -> Workspace {
        Workspace::new(self.git_path.parent().expect(".git dir has no parent"))
    }
}
