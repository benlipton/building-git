pub mod cmd;
mod database;
mod index;
mod lockfile;
mod refs;
pub mod repository;
pub mod telemetry;
mod workspace;

use anyhow::Result;

pub fn run() -> Result<()> {
    telemetry::init();

    let cli_cmd = cmd::parse();
    let mut process_info = cmd::ProcessInfo::from_env();
    cmd::execute(cli_cmd, &mut process_info)
}
