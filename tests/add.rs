use std::fs;
use std::io;
use std::os::unix::fs::PermissionsExt;
use std::path::{Path, PathBuf};

use jit::repository::Repository;
use tempfile::{tempdir, TempDir};

use jit::cmd;
use jit::repository;
use jit::telemetry;

struct TestHelper {
    root: TempDir,
    stdout: Vec<u8>,
    stderr: Vec<u8>,
}

impl TestHelper {
    fn new() -> Self {
        telemetry::init();

        let tempdir = tempdir().expect("tempdir");

        let mut helper = Self {
            root: tempdir,
            stdout: Vec::new(),
            stderr: Vec::new(),
        };

        helper.init_repo();

        tracing::debug!(path = ?helper.workspace_root(), "Initialized helper");

        helper
    }

    fn repo_root(&self) -> PathBuf {
        self.root.path().join(".git")
    }

    fn workspace_root(&self) -> PathBuf {
        self.root.path().to_owned()
    }

    fn with_process_info<R>(&mut self, f: impl FnOnce(cmd::ProcessInfo) -> R) -> R {
        let root = self.workspace_root();
        let stdout = Box::new(io::Cursor::new(&mut self.stdout));
        let stderr = Box::new(io::Cursor::new(&mut self.stderr));
        let process_info = cmd::ProcessInfo {
            root,
            stdin: Box::new(io::empty()),
            stdout,
            stderr,
        };
        f(process_info)
    }

    fn repo(&self) -> repository::Repository {
        repository::Repository::new(self.repo_root())
    }

    fn write_file(&self, path: impl AsRef<Path>, contents: &str) {
        let path = self.workspace_root().join(path);
        fs::create_dir_all(path.parent().unwrap()).expect("create_dir_all");
        fs::write(path, contents).expect("write_file");
    }

    fn init_repo(&mut self) {
        let root = self.workspace_root();
        self.with_process_info(|mut process_info| {
            cmd::execute(cmd::Cli::Init(cmd::init::Args { root }), &mut process_info)
        })
        .expect("init");
        self.clear_stdio();
    }

    fn make_executable(&self, path: impl AsRef<Path>) {
        let path = self.workspace_root().join(path);
        let mut perms = fs::metadata(&path).expect("metadata").permissions();
        perms.set_mode(0o755);
        fs::set_permissions(&path, perms).expect("set_permissions");
    }

    fn clear_stdio(&mut self) {
        self.stdout.clear();
        self.stderr.clear();
    }

    fn assert_stdout(&self, expected: &[u8]) {
        assert_eq!(
            self.stdout.as_slice(),
            expected,
            "left: ({}) right: ({})",
            String::from_utf8_lossy(self.stdout.as_slice()),
            String::from_utf8_lossy(expected)
        );
    }

    fn assert_stderr(&self, expected: &[u8]) {
        assert_eq!(
            self.stderr.as_slice(),
            expected,
            "left: ({}) right: ({})",
            String::from_utf8_lossy(self.stderr.as_slice()),
            String::from_utf8_lossy(expected)
        );
    }
}

fn assert_index(repo: &Repository, expected: Vec<(u32, String)>) {
    let index = repo.index().expect("index");
    let index_items: Vec<_> = index
        .iter()
        .map(|entry| (entry.mode, entry.path.clone()))
        .collect();
    assert_eq!(index_items, expected);
}

#[test]
fn can_add_regular_file_to_index() {
    let mut helper = TestHelper::new();

    helper.write_file("hello.txt", "hello");

    let cmd = cmd::Cli::Add(cmd::add::Args {
        paths: vec!["hello.txt".into()],
    });
    helper
        .with_process_info(|mut process_info| cmd::execute(cmd, &mut process_info))
        .expect("add");

    let repo = helper.repo();
    assert_index(&repo, vec![(0o100644, "hello.txt".to_owned())]);
}

#[test]
fn can_add_executable_file_to_index() {
    let mut helper = TestHelper::new();

    helper.write_file("hello.txt", "hello");
    helper.make_executable("hello.txt");

    let cmd = cmd::Cli::Add(cmd::add::Args {
        paths: vec!["hello.txt".into()],
    });
    helper
        .with_process_info(|mut process_info| cmd::execute(cmd, &mut process_info))
        .expect("add");

    let repo = helper.repo();
    assert_index(&repo, vec![(0o100755, "hello.txt".to_owned())]);
}

#[test]
fn can_add_multiple_files_to_index() {
    let mut helper = TestHelper::new();

    helper.write_file("hello.txt", "hello");
    helper.write_file("world.txt", "world");

    let cmd = cmd::Cli::Add(cmd::add::Args {
        paths: vec!["hello.txt".into(), "world.txt".into()],
    });
    helper
        .with_process_info(|mut process_info| cmd::execute(cmd, &mut process_info))
        .expect("add");

    let repo = helper.repo();
    assert_index(
        &repo,
        vec![
            (0o100644, "hello.txt".to_owned()),
            (0o100644, "world.txt".to_owned()),
        ],
    );
}

#[test]
fn can_incrementally_add_files_to_index() {
    let mut helper = TestHelper::new();

    helper.write_file("hello.txt", "hello");
    helper.write_file("world.txt", "world");

    let cmd = cmd::Cli::Add(cmd::add::Args {
        paths: vec!["hello.txt".into()],
    });
    helper
        .with_process_info(|mut process_info| cmd::execute(cmd, &mut process_info))
        .expect("add");

    let repo = helper.repo();
    assert_index(&repo, vec![(0o100644, "hello.txt".to_owned())]);

    let cmd = cmd::Cli::Add(cmd::add::Args {
        paths: vec!["world.txt".into()],
    });
    helper
        .with_process_info(|mut process_info| cmd::execute(cmd, &mut process_info))
        .expect("add");

    assert_index(
        &repo,
        vec![
            (0o100644, "hello.txt".to_owned()),
            (0o100644, "world.txt".to_owned()),
        ],
    );
}

#[test]
fn can_add_directory_to_index() {
    let mut helper = TestHelper::new();

    helper.write_file("a-dir/nested.txt", "content");

    let cmd = cmd::Cli::Add(cmd::add::Args {
        paths: vec!["a-dir".into()],
    });
    helper
        .with_process_info(|mut process_info| cmd::execute(cmd, &mut process_info))
        .expect("add");

    let repo = helper.repo();
    assert_index(&repo, vec![(0o100644, "a-dir/nested.txt".into())]);
}

#[test]
fn can_add_repository_root_to_index() {
    let mut helper = TestHelper::new();

    helper.write_file("a/b/c/file.txt", "content");

    let cmd = cmd::Cli::Add(cmd::add::Args {
        paths: vec![".".into()],
    });
    helper
        .with_process_info(move |mut process_info| cmd::execute(cmd, &mut process_info))
        .expect("add");

    let repo = helper.repo();
    assert_index(&repo, vec![(0o100644, "a/b/c/file.txt".into())]);
}

#[test]
fn is_silent_on_success() {
    let mut helper = TestHelper::new();

    helper.write_file("hello.txt", "hello");

    let cmd = cmd::Cli::Add(cmd::add::Args {
        paths: vec!["hello.txt".into()],
    });
    helper
        .with_process_info(|mut process_info| cmd::execute(cmd, &mut process_info))
        .expect("add");

    helper.assert_stdout(b"");
    helper.assert_stderr(b"");
}
